$(function (argument) {
	$('a.templates-toggle').on('click', function () {
		var $this = $(this);
		var action = $this.attr('action');
		var accID = $this.attr('accordion-id');

		$('#' + accID).find('.panel-collapse').each(function (argument) {
			$(this).collapse(action);
		});

		//$('#' + accID).find('.panel-collapse').collapse(action);
		$this.siblings('.templates-toggle').removeClass('text-muted');
		$this.addClass('text-muted');
	});

	$('.input-group.date').datepicker({
	    autoclose: true,
	    todayHighlight: true
	});


	$('[role="number-range"]').each(function () {
		var $this = $(this);
		var $input = $this.find('input.form-control');

		var min = window.parseFloat($input.attr('data-min'));
		var max = window.parseFloat($input.attr('data-max'));
		var isSet = function (target) {
			return typeof target != 'undefined';
		}

		console.log(min)
		console.log(max)

		$this.find('[aria-controls]').on('click', function (argument) {
			if ($(this).attr('aria-controls') == 'increase') {
				$this.trigger('range.bs.increase');
			}

			if ($(this).attr('aria-controls') == 'decrease') {
				$this.trigger('range.bs.decrease');
			}
		})

		$input.on('focusout', function (argument) {
			var value = $input.val();

			if (window.isNaN(value) || !value.length) {
				value = 0;
			};

			if (isSet(max) && value > max) {
				$input.val(max)
			};

			if (isSet(min) && value < min) {
				$input.val(min)
			};
		})

		$this.on('range.bs.increase', function (argument) {
			var value = $input.val();

			if (window.isNaN(value) || !value.length) {
				value = 0;
			};

			var next = window.parseFloat(value) + 1;

			if (!window.isNaN(max)) {
				if (next <= max) {
					$input.val(next);
				}

				return;
			}

			$input.val(next);
			/*if (isSet(max) && next <= max) {
				$input.val(next);
			}*/
		});

		$this.on('range.bs.decrease', function (argument) {
			var value = $input.val();

			if (window.isNaN(value) || !value.length) {
				value = 0;
			};

			var prev = window.parseFloat(value) - 1;

			if (!window.isNaN(min)) {
				if (prev >= min) {
					$input.val(prev);
				};

				return;
			}

			$input.val(prev);

			/*if (isSet(min) && prev >= min) {
				$input.val(prev);
			}*/
		});
	})
})